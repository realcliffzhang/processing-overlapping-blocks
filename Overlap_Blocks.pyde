from random import randint

def setup():
    global color_groups, time
    background(255)
    size(700, 700)
    color_groups = {}
    time = millis()

def draw():
    global time
    if millis() > time + 400:
        time = millis()
        x, y, w, h = randint(100, 500), randint(100, 500), randint(50, 100), randint(50, 100)
        rand_color = (randint(0, 255), randint(0, 255), randint(0, 255))
        while rand_color in color_groups.keys():
            rand_color = (randint(0, 255), randint(0, 255), randint(0, 255))
        overlapped_colors = []
        for color_group in color_groups.items():
            cur_color = color_group[0]
            print(cur_color)
            cur_rects = color_group[1]
            print(cur_rects)
            cur_overlap = False
            for cur_rect in cur_rects:
                overlapped = check_overlap(x, y, x + w, y + h, cur_rect[0], cur_rect[1], cur_rect[0] + cur_rect[2], cur_rect[1] + cur_rect[3])
                if overlapped:
                    cur_overlap = True
                    # break
            if cur_overlap:
                overlapped_colors.append(cur_color)
        if len(overlapped_colors) == 0:
            print("no overlap")
            fill(*rand_color)
            rect(x, y, w, h)
            color_groups[rand_color] = [[x, y, w, h]]
        elif len(overlapped_colors) == 1:
            print("one overlap")
            fill(*overlapped_colors[0])
            rect(x, y, w, h)
            color_groups[overlapped_colors[0]].append([x, y, w, h])
        else:
            print("two or more overlap")
            prev_rects = []
            for overlapped_color in overlapped_colors:
                for prev_rect in color_groups[overlapped_color]:
                    prev_rects.append(prev_rect)
            for overlapped_color in overlapped_colors:
                del color_groups[overlapped_color]
            color_groups[rand_color] = []
            for prev_rect in prev_rects:
                rect(*prev_rect)
                color_groups[rand_color].append(prev_rect)
            rect(x, y, w, h)
            color_groups[rand_color].append([x, y, w, h])
        clear()
        background(255)
        for color_key in color_groups.keys():
            fill(*color_key)
            for a_rect in color_groups[color_key]:
                rect(*a_rect)

def check_overlap(top_left_1_x, top_left_1_y, bottom_right_1_x, bottom_right_1_y, top_left_2_x, top_left_2_y, bottom_right_2_x, bottom_right_2_y):
    if top_left_1_x > bottom_right_2_x or top_left_2_x > bottom_right_1_x:
        return False
    if top_left_1_y > bottom_right_2_y or top_left_2_y > bottom_right_1_y:
        return False
    return True
